# Launches the Orderer
# Filedger Location specified in the YAML file.
# You may override this
export ORDERER_FILELEDGER_LOCATION=$HOME/ledgers/orderer/uni-net/ledger
# Change this to control logs verbosity
export FABRIC_LOGGING_SPEC=INFO
#### Just in case this variable is not set ###
export FABRIC_CFG_PATH=$PWD
# Launch orderer
orderer