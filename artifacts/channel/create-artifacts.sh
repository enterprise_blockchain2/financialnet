# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/hlf_financial_service/fabric-samples/bin/" ] ; then
    PATH="/workspaces/hlf_financial_service/fabric-samples/bin/:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm genesis.block mychannel.tx
rm -rf ../../channel-artifacts/*


#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=/workspaces/financialnet/artifacts/channel


# generating genesis block for the BankingConsortium orderer 
configtxgen -outputBlock financial-genesis.block -profile NatbankOrdererGenesis -channelID ordererchannel


# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx financial-channel.tx -profile NatbankChannel -channelID natbankchannel


